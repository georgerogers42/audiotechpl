package Audiotech::Controller::Posts;
use Mojo::Base qw(Mojolicious::Controller);
use Audiotech::Model;

sub view {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  my $cuser = $conn->resultset('User')->find({username => $self->session('username')});
  my $post = $conn->resultset('Post')->find({id => $self->stash('id')});
  $self->stash(post => $post, cuser => $cuser);
  $self->render;
}

sub create {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  my $user = $conn->resultset('User')->find({ username => $self->session('username')});
  if($user) {
    my $post = $conn->resultset('Post')->create({title => $self->param('title'),
                                                 contents => $self->param('contents'),
                                                 user_id => $user->id});
    $self->flash(success => "Created new Post");
    $self->redirect_to('post', id => $post->id);
  } else {
    $self->redirect_to('home');
  }
}

sub edit {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  my $user = $conn->resultset('User')->find({ username => $self->session('username')});
  if($user) {
    my $post = $conn->resultset('Post')->find({id => $self->stash('id'), 'user_id' => $user->id});
    $post->update({title => $self->param('title'), contents => $self->param('contents')});
    $self->flash(success => "Updated Post");
    $self->redirect_to('post', id => $post->id);
  } else {
    $self->redirect_to('home');
  }
}

1;
