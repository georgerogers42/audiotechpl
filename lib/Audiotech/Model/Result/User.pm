package Audiotech::Model::Result::User;
use strictures;
use utf8;
use Crypt::Argon2 qw(argon2id_pass argon2id_verify);
use Data::Entropy::Algorithms qw(rand_bits);
use parent qw(DBIx::Class::Core);

__PACKAGE__->table('users');

__PACKAGE__->add_columns(qw(id email username name password));

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(posts => ('Audiotech::Model::Result::Post', 'user_id'));

sub set_password {
  my ($self, $clearpass) = @_;
  $self->password(argon2id_pass($clearpass, rand_bits(16*8), 3, '32M', 1, 16));
}

sub has_password {
  my ($self, $clearpass) = @_;
  return argon2id_verify($self->password, $clearpass);
}

sub long_email {
  my $self = shift;
  return "@{[$self->name]} <@{[$self->email]}>";
}

1;
