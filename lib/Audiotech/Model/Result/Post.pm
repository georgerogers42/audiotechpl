package Audiotech::Model::Result::Post;
use strictures;
use utf8;
use parent qw(DBIx::Class::Core);

__PACKAGE__->table('posts');

__PACKAGE__->add_columns(qw(id user_id title contents));

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(user => ('Audiotech::Model::Result::User', 'user_id'));

1;
