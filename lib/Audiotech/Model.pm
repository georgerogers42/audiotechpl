package Audiotech::Model;
use strictures;
use parent qw(DBIx::Class::Schema);

__PACKAGE__->load_namespaces;

sub get_connection {
  my $self = shift;
  $self->connect($ENV{DBI_STRING} // "dbi:Pg:database=audiotech", $ENV{DBI_USER}, $ENV{DBI_PASS}, { AutoCommit => 1});
}

1;
