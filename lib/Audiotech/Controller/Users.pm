package Audiotech::Controller::Users;
use Try::Tiny;
use Mojo::Base 'Mojolicious::Controller';
use Audiotech::Model;

sub welcome {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  my $user = $conn->resultset('User')->find({username => $self->session('username')});
  $self->stash(users => [$conn->resultset('User')->all], cuser => $user);
  return $self->render;
}

sub user {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  $self->stash(user => $conn->resultset('User')->find({id => $self->stash('id')}),
               cuser => $conn->resultset('User')->find({username => $self->session('username')}));
  return $self->render;
}

sub login {
  my $self = shift;
  my $conn = Audiotech::Model->get_connection;
  my $user = $conn->resultset('User')->find({username => $self->param('username')});
  if($user && $user->has_password($self->param('password'))) {
    $self->session(username => $user->username);
    $self->flash(success => "Logged in as @{[$user->username]}");
    return $self->redirect_to('user', id => $user->id);
  } else {
    $self->flash(errors => "Cannot log in");
    return $self->redirect_to('home');
  }
}

sub signup {
  my $self = shift;
  try {
    my $conn = Audiotech::Model->get_connection;
    my $user = $conn->resultset('User')->new({});
    $user->email($self->param('email'));
    $user->name($self->param('name'));
    $user->username($self->param('username'));
    $user->set_password($self->param('password'));
    $user->insert;
    $self->flash(success => 'Succesfully created user');
    return $self->redirect_to('home');
  } catch {
    if($_->isa('DBIx::Class::Exception')) {
      $self->flash({'errors', 'Error Creating User'});
      $self->redirect_to('home');
    } else {
      die $_;
    }
  };
}

sub logout {
  my $self = shift;
  $self->session(expires => 1);
  $self->redirect_to('home');
}

1;
