package Audiotech;
use Mojo::Base 'Mojolicious';


# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by "my_app.conf"
  my $config = $self->plugin('Config');

  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer') if $config->{perldoc};

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('users#welcome')->name('home');
  $r->get('/user/:id')->to('users#user')->name('user');
  $r->get('/post/:id')->to('posts#view')->name('post');
  $r->post('/post/:id/edit')->to('posts#edit')->name('edit_post');
  $r->post('/post/new')->to('posts#create')->name('create_post');
  $r->post('/login')->to('users#login')->name('login');
  $r->post('/logout')->to('users#logout')->name('logout');
  $r->post('/signup')->to('users#signup')->name('signup');
}

1;
